import 'dotenv/config';
import * as request from 'supertest';
import * as mongoose from 'mongoose';
import { app } from './constants';
import axios from 'axios';
import { RegisterDTO } from '../src/auth/auth.dto';
import { CreateProductDTO } from '../src/product/product.dto';
import { HttpStatus } from '@nestjs/common';

if (process.env.NODE_ENV === 'test') {
  process.env.MONGO_URI = process.env.MONGO_URI_TEST;
}

let sellerToken: string;
const productSeller: RegisterDTO = {
  seller: true,
  username: 'productSeller',
  password: 'password',
};

beforeAll(async () => {
  await mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true });
  await mongoose.connection.db.dropDatabase();

  const {data: {token}} = await axios.post(`${app}/auth/register`, productSeller);
  sellerToken = token;
});

afterAll(async done => {
  await mongoose.disconnect(done);
});

describe('PRODUCT', () => {
  const product: CreateProductDTO = {
    title: 'Some Product Title',
    description: 'Some Product Descr',
    image: 'n/a',
    price: 250,
  };
  let productId: string;

  it('should create product', () => {
    return request(app)
      .post('/product')
      .set('Authorization', `Bearer ${sellerToken}`)
      .set('Accept', 'application/json')
      .send(product)
      .expect(({body}) => {
        expect(body._id).toBeDefined();
        productId = body._id;
        expect(body.title).toEqual(product.title);
        expect(body.description).toEqual(product.description);
        expect(body.price).toEqual(product.price);
        expect(body.owner.username).toEqual(productSeller.username);
      })
      .expect(HttpStatus.CREATED);
  });

  it('should list all products', () => {
    return request(app)
      .get('/product')
      .expect(200);
  });

  it('should list my products', () => {
    return request(app)
      .get('/product/mine')
      .set('Authorization', `Bearer ${sellerToken}`)
      .expect(200);
  });

  it('should read product', () => {
    return request(app)
      .get(`/product/${productId}`)
      .expect(({body}) => {
        expect(body._id).toEqual(productId);
        expect(body.title).toEqual(product.title);
        expect(body.description).toEqual(product.description);
        expect(body.price).toEqual(product.price);
        expect(body.image).toEqual(product.image);
        expect(body.owner.username).toEqual(productSeller.username);
      })
      .expect(200);
  });

  it('should update product', () => {
    return request(app)
      .put(`/product/${productId}`)
      .set('Authorization', `Bearer ${sellerToken}`)
      .set('Accept', 'application/json')
      .send({title: 'New title'})
      .expect(({body}) => {
        expect(body.title).not.toEqual(product.title);
        expect(body.description).toEqual(product.description);
        expect(body.price).toEqual(product.price);
        expect(body.owner.username).toEqual(productSeller.username);
      })
      .expect(200);
  });

  it('should delete product', async () => {
    await axios.delete(`${app}/product/${productId}`, {
      headers: {Authorization: `Bearer ${sellerToken}`},
    });

    return request(app)
      .get(`/product/${productId}`)
      .expect(HttpStatus.NO_CONTENT);
  });
});
