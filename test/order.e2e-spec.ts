import 'dotenv/config';
import * as request from 'supertest';
import * as mongoose from 'mongoose';
import { app } from './constants';
import axios from 'axios';
import { RegisterDTO } from '../src/auth/auth.dto';
import { CreateProductDTO } from '../src/product/product.dto';
import { HttpStatus } from '@nestjs/common';
import { Product } from '../src/types/product';

if (process.env.NODE_ENV === 'test') {
  process.env.MONGO_URI = process.env.MONGO_URI_TEST;
}

let sellerToken: string;
let buyerToken: string;
let bouthProducts: Product[];
const orderBuyer: RegisterDTO = {
  seller: false,
  username: 'orderBuyer',
  password: 'password',
};
const orderSeller: RegisterDTO = {
  seller: true,
  username: 'productSeller',
  password: 'password',
};
const soldProducts: CreateProductDTO[] = [
  {
    title: 'new phone',
    image: 'n/a',
    description: 'some description new phone',
    price: 180,
  },
  {
    title: 'old phone',
    image: 'n/a',
    description: 'some description old phone',
    price: 80,
  },
];

beforeAll(async () => {
  await mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true });
  await mongoose.connection.db.dropDatabase();

  ({data: {token: sellerToken}} = await axios.post(`${app}/auth/register`, orderSeller));
  ({data: {token: buyerToken}} = await axios.post(`${app}/auth/register`, orderBuyer));

  const [{data: data1}, {data: data2}] = await Promise.all(
    soldProducts.map(product => axios.post(`${app}/product`, product, {headers: {authorization: `Bearer ${sellerToken}`}})),
  );

  bouthProducts = [data1, data2];
});

afterAll(async done => {
  await mongoose.disconnect(done);
});

describe('ORDER', () => {
  it('should create order of all products', () => {
    const orderDTO = {
      product: bouthProducts.map(product => ({
        product: product._id,
        quantity: 1,
      })),
    };

    return request(app)
      .post('/order')
      .send(orderDTO)
      .set('Authorization', `Bearer ${buyerToken}`)
      .set('Accept', 'application/json')
      .expect(({body}) => {
        console.log(body);
        expect(body.owner.username).toEqual(orderBuyer.username);
        expect(body.products.length).toEqual(bouthProducts.length);
        expect(bouthProducts.map(product => product._id).includes(body.products[0]._id)).toBeTruthy();
        expect(body.totalPrice).toEqual(bouthProducts.reduce((acc, product) => acc + product.price, 0));
      })
      .expect(201);
  });

  it('should list all orders of buyer', () => {
    return request(app)
      .get('/order')
      .set('Authorization', `Bearer ${buyerToken}`)
      .expect(({body}) => {
        expect(body.length).toEqual(1);
        expect(body[0].products.length).toEqual(bouthProducts.length);
        expect(bouthProducts.map(product => product._id).includes(body[0].products[0]._id)).toBeTruthy();
      });
  });
});
