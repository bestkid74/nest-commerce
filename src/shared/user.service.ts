import * as bcrypt from 'bcrypt';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { User } from '../types/user';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { LoginDTO, RegisterDTO } from '../auth/auth.dto';

@Injectable()
export class UserService {
  constructor(@InjectModel('User') private userModel: Model<User>) {}

  sanitizeUser(user: User) {
    return user.depopulate('password');
  }

  async create(userDTO: LoginDTO) {
    const {username} = userDTO;
    const user = await this.userModel.findOne({username});
    if (user) {
      throw new HttpException('User already exist', HttpStatus.BAD_REQUEST);
    }

    const createdUser = new this.userModel(userDTO);
    await createdUser.save();
    return this.sanitizeUser(createdUser);
  }

  async findByLogin(userDTO: RegisterDTO) {
    const {username, password} = userDTO;
    const user = await this.userModel.findOne({username});

    if (!user) {
      throw new HttpException('Invalid credentials', HttpStatus.UNAUTHORIZED);
    }
    if (await bcrypt.compare(password, user.password)) {
      return this.sanitizeUser(user);
    } else {
      throw new HttpException('Invalid credentials', HttpStatus.UNAUTHORIZED);
    }
  }

  async findByPayload(payload: any) {
    const {username} = payload;
    return await this.userModel.findOne({username});
  }
}
